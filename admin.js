var app     = require('express')()
const path  = require("path");


// PUG SPECIFIC STUFF
app.set('view engine', 'pug') // Set the template engine as pug
app.set('views', path.join(__dirname, 'views')) // Set the views directory


app.get('/', function (req, res) {
    const params = {'user_name': 'Admin'}
    res.status(200).render('admin/view_dashboard.pug', params);
});

app.get('/create_test', function (req, res) {
    const QueryBuilder = require('node-querybuilder');
    const settings = {
        host: 'localhost',
        user: 'rupesh',
        password: 'chutiya',
        database: 'onlinetest'
    };
    const pool = new QueryBuilder(settings, 'mysql', 'pool');
    
    pool.get_connection(qb => {
        qb.select('id,name')
            .get('ot_test_list', (err, response) => {
                qb.release();
    
                if (err) 
                    return console.error("Uh oh! Couldn't get results: " + err.msg);
    
                console.log("Query Ran: " + qb.last_query());
                console.log("Results:", response);
                res.send(response);

            }
        );
    });
});

app.get('/add_modify_que', function (req, res) {
    const params = {'user_name': 'Admin'}
    res.status(200).render('admin/view_dashboard.pug', params);
});

app.listen(3000, function () {
    //console will print in terminal on change of file
    console.log('Example app listsening on port 3000!');
});
